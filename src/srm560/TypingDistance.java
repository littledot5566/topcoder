package srm560;

public class TypingDistance {
	public int minDistance(String keyboard, String word) {

		// index the keyboard
		int[] pos = new int[26];
		for (int i = 0; i < keyboard.length(); i++) {
			pos[keyboard.charAt(i) - 97] = i;
		}

		int start = pos[word.charAt(0) - 97];
		int moves = 0;

		for (int i = 1; i < word.length(); i++) {
			int end = pos[word.charAt(i) - 97];
			moves += Math.abs(end - start);
			start = end;
		}

		return moves;
	}
}