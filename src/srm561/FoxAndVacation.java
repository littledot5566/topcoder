package srm561;

import java.util.Arrays;

public class FoxAndVacation {
	public int maxCities(int total, int[] d) {
		Arrays.sort(d);

		int days = 0;
		int cities = 0;
		for (int i = 0; i < d.length; i++) {
			days += d[i];
			if (days > total)
				break;
			cities++;
		}
		return cities;
	}
}