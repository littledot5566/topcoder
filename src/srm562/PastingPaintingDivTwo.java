package srm562;

/*
Problem Statement
����
Cucumber Boy likes drawing pictures. Today, he plans to draw a picture using a very simple graphics editor.

The editor has the following functions:
The canvas is an infinite two-dimensional grid of pixels.
There are only two colors: black, and transparent. These are denoted 'B' and '.' (a period), respectively.
The editor has a clipboard that contains a rectangular picture.
The editor can take the picture in the clipboard and paste it onto any corresponding rectangle of the canvas. The user just has to select the pixel of the canvas where the upper left corner of the clipboard will be pasted.
When pasting the picture, the black pixels of the picture in the clipboard will overwrite their corresponding pixels on the canvas. The pixels that are transparent in the clipboard picture do not change the canvas.

At this moment, all pixels on the infinite canvas are transparent. Cucumber Boy has already stored a picture in the clipboard. You are given this picture as a String[] clipboard.

Cucumber Boy now wants to paste the clipboard picture onto the canvas exactly T times in a row.
For each i, when pasting the clipboard for the i-th time, he will choose the pixel (i,i) as the upper left corner of the pasted picture.

You are given the String[] clipboard and the int T. Return the number of black pixels on the canvas after all the pasting is finished.
Definition
����
Class:
PastingPaintingDivTwo
Method:
countColors
Parameters:
String[], int
Returns:
long
Method signature:
long countColors(String[] clipboard, int T)
(be sure your method is public)
����

Constraints
-
clipboard will contain between 1 and 50 elements, inclusive.
-
Each element of clipboard will contain between 1 and 50 characters, inclusive.
-
Each element of clipboard will contain the same number of characters.
-
Each character of each element of clipboard will be 'B' or '.'.
-
T will be between 1 and 1,000,000,000, inclusive.
Examples
0)

����
{
"..B",
"B..",
"BB."
}
3
Returns: 10
 
1)

����
{
"B...",
"....",
"....",
"...B"
}
2
Returns: 4

2)

����
{"BBB"}
10000
Returns: 30000

3)

����
{"."}
1000000000
Returns: 0

4)

����
{
"BB.",
".B."
}
100
Returns: 201

5)

����
{
"..........B..........",
".........B.B.........",
"........B...B........",
".......B.....B.......",
"......B..B.B..B......",
".....B...B.B...B.....",
"....B...........B....",
"...B...B.....B...B...",
"..B.....BBBBBB....B..",
".B..........BB.....B.",
"BBBBBBBBBBBBBBBBBBBBB"
}
1000000000
Returns: 21000000071
Note that the answer may overflow a 32-bit integer variable.

 
This is the image of clipboard in this example.
This problem statement is the exclusive and proprietary property of TopCoder, Inc. Any unauthorized use or reproduction of this information without the prior written consent of TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved.
 */

public class PastingPaintingDivTwo {
	int w, h, x, y, max;
	int[] pattern;
	long totalBlack = 0;

	public long countColors(String[] clipboard, int T) {

		w = clipboard[0].length();
		h = clipboard.length;

		max = w < h ? w : h;
		pattern = new int[50];

		// traverse diagonally: left->right
		for (int i = 0; i < w; i++) {
			clear(pattern);
			for (int j = 0; j < h && j + i < w; j++) {
				x = i + j;
				y = j;
				if (clipboard[y].charAt(x) == 'B')
					pattern[j] = 1;
			}
			totalBlack += countBlack(pattern, T);
		}

		// traverse diagonally: top->bottom
		for (int j = 1; j < h; j++) {
			clear(pattern);
			for (int i = 0; i < w && i + j < h; i++) {
				x = i;
				y = i + j;
				if (clipboard[y].charAt(x) == 'B')
					pattern[i] = 1;
			}
			totalBlack += countBlack(pattern, T);
		}

		return totalBlack;
	}

	private void clear(int[] arr) {
		for (int i = 0; i < arr.length; i++)
			arr[i] = 0;
	}

	private long countBlack(int[] pat, int T) {
		// debug pattern
		System.err.format("pattern=");
		for (int i = 0; i < max; i++) {
			System.err.format("" + pat[i]);
		}
		System.err.format("\n");

		boolean firstBlack = true;
		int white = 0;
		long black = 0;

		for (int i = max - 1; i > -1; i--) {
			if (pat[i] == 1) {
				if (firstBlack) {
					// last black pixel will always be printed T times
					black += T;
					firstBlack = false;
				} else {
					// latter black pixels can only be printed a maximum of white + 1
					// times
					black += T > white + 1 ? white + 1 : T;
				}
				white = 0;
			} else if (pat[i] == 0)
				white++;
		}

		System.err.println("black=" + black);

		return black;
	}
}
