package srm563;

public class CoinsGameEasy {
	public int minimalSteps(String[] b) {
		return new Recursive().minimalSteps(b);
	}

	private class Recursive {
		private int				x1				= -1, y1 = -1, x2 = -1, y2 = -1;
		private int				h, w;
		private int				minSteps	= 11;
		private String[]	board;

		public int minimalSteps(String[] b) {
			// get board dimensions
			board = b;
			h = b.length;
			w = b[0].length();

			// get coin positions
			for (int i = 0; i < h; i++) {
				for (int j = 0; j < w; j++) {
					if (b[i].charAt(j) == 'o') {
						if (x1 == y1 && x1 == -1) {
							y1 = i;
							x1 = j;
						} else {
							y2 = i;
							x2 = j;
						}
					}
				}
			}
			System.err.format("c1=(%d %d) c2=(%d %d) w=%d h=%d\n",
					x1, y1, x2, y2, w, h);

			// begin recursing
			int n = 0;
			step(x1, y1, x2, y2, 1, 0, n + 1);
			step(x1, y1, x2, y2, 0, -1, n + 1);
			step(x1, y1, x2, y2, -1, 0, n + 1);
			step(x1, y1, x2, y2, 0, 1, n + 1);

			return minSteps == 11 ? -1 : minSteps;
		}

		private void step(int x1, int y1, int x2, int y2, int dx, int dy, int n) {
			x1 += dx;
			y1 += dy;
			x2 += dx;
			y2 += dy;

			// one coin falls
			if (x1 < 0 || x1 > w - 1 || y1 < 0 || y1 > h - 1 ||
					x2 < 0 || x2 > w - 1 || y2 < 0 || y2 > h - 1) {
				if (!((x1 < 0 || x1 > w - 1 || y1 < 0 || y1 > h - 1) && (x2 < 0
						|| x2 > w - 1 || y2 < 0 || y2 > h - 1))) {
					if (n < minSteps) {
						minSteps = n;
					}
				}
				return;
			}

			// coin hits obstacle
			if (board[y1].charAt(x1) == '#') {
				x1 -= dx;
				y1 -= dy;
			}
			if (board[y2].charAt(x2) == '#') {
				x2 -= dx;
				y2 -= dy;
			}

			// coin overlaps
			if (x1 == x2 && y1 == y2) {
				return;
			}

			// too many steps
			if (n + 1 <= 10 || n + 1 < minSteps) {
				step(x1, y1, x2, y2, 1, 0, n + 1);
				step(x1, y1, x2, y2, 0, -1, n + 1);
				step(x1, y1, x2, y2, -1, 0, n + 1);
				step(x1, y1, x2, y2, 0, 1, n + 1);
			}
		}

	}

}