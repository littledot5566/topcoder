package srm563;

public class SpellCardsEasy {
	public int maxDamage(int[] level, int[] damage) {
		return new Dyn().maxDamage(level, damage);
	}

}

class Dyn2 {
	int	DMG	= 0;

	public int maxDamage(int[] level, int[] damage) {

		return DMG;
	}

}

class Dyn {
	int[]	level, damage;
	int		DMG	= 0;

	public int maxDamage(int[] level, int[] damage) {
		this.level = level;
		this.damage = damage;

		int maxDMG = 0;
		for (int dmg : damage)
			maxDMG += dmg;

		boolean playable = play(0, 0, 0, maxDMG);
		if (playable)
			skip(0, 0, 0, maxDMG);

		return DMG;
	}

	private boolean play(int pos, int req, int dmg, int max) {
		boolean played = true;
		if (level.length - pos - req >= level[pos]) {
			req += level[pos] - 1;
			dmg += damage[pos];

			if (dmg > DMG)
				DMG = dmg;
		} else {
			played = false;
			if (req > 0)
				req--;
		}

		int maxDMG = max - damage[pos];

		if (pos + 1 < level.length && maxDMG > DMG - dmg) {
			boolean playable = play(pos + 1, req, dmg, maxDMG);
			if (playable)
				skip(pos + 1, req, dmg, maxDMG);
		}

		return played;
	}

	private void skip(int pos, int req, int dmg, int max) {
		int maxDMG = max - damage[pos];

		if (maxDMG > DMG - dmg) {
			if (req > 0)
				req--;
			if (pos + 1 < level.length) {
				play(pos + 1, req, dmg, maxDMG);
				skip(pos + 1, req, dmg, maxDMG);
			}
		}
	}
}

class Rec {
	private int	DMG	= 0;
	private int[]	L, D;
	private int		N;

	public int maxDamage(int[] level, int[] damage) {
		L = level;
		D = damage;
		N = level.length;
		int[] H = new int[N];

		// start recursing
		for (int i = 0; i < N; i++)
			play(H.clone(), 0, i);

		return DMG;
	}

	private void play(int[] H, int dmg, int pos) {
		// card is already discarded
		if (H[pos] == 1)
			return;

		// count no of cards on the right
		int right = 0;
		for (int i = pos; i < N; i++)
			if (H[i] == 0)
				right++;

		// too little cards
		if (right < L[pos])
			return;

		// deal dmg
		dmg += D[pos];
		if (dmg > DMG)
			DMG = dmg;

		// discard hand
		int discarded = 0;
		for (int i = pos; i < N; i++) {
			if (H[i] == 0) {
				H[i] = 1;

				if (++discarded == L[pos])
					break;
			}
		}

		// play next card
		if (pos + 1 < N)
			for (int i = 0; i < N; i++)
				play(H.clone(), dmg, i);
	}
}