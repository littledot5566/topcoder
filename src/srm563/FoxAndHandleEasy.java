package srm563;

public class FoxAndHandleEasy
{
	public String isPossible(String S, String T)
	{
		int pos = 0;
		while (true) {
			pos = T.indexOf(S, pos + 1);

			if (pos == -1)
				return "No";

			String firstHalf = T.substring(0, pos);
			String secHalf = T.substring(pos + S.length(), T.length());

			System.err.format("f=%s s=%s pos=%d\n", firstHalf, secHalf, pos);

			if (S.compareTo(firstHalf.concat(secHalf)) == 0)
				return "Yes";
		}
	}
}