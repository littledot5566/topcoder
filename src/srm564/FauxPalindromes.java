package srm564;

public class FauxPalindromes
{
	public String classifyIt(String word)
	{
		String[] halves = cut(word);

		if (halves[0].compareTo(reverse(halves[1])) == 0)
			return "PALINDROME";

		halves = cut(trim(word));

		if (halves[0].compareTo(reverse(halves[1])) == 0)
			return "FAUX";

		return "NOT EVEN FAUX";
	}

	/**
	 * Split the string into halves, not including the middle character if exists.
	 * 
	 * @param word
	 * @return
	 */
	private String[] cut(String word) {
		int half = word.length() / 2;
		boolean even = word.length() % 2 == 0;
		String firstHalf = word.substring(0, half);
		String secHalf = word.substring(even ? half : half + 1, word.length());

		return new String[] { firstHalf, secHalf };
	}

	/**
	 * Reverse the characters in the string.
	 * 
	 * @param s
	 * @return
	 */
	private String reverse(String s) {
		StringBuilder sb = new StringBuilder();

		for (int i = s.length() - 1; i > -1; i--)
			sb.append(s.charAt(i));

		return sb.toString();
	}

	/**
	 * Remove repetitive characters.
	 * 
	 * @param s
	 * @return
	 */
	private String trim(String s) {
		StringBuilder sb = new StringBuilder("" + s.charAt(0));

		for (int i = 1; i < s.length(); i++) {
			if (s.charAt(i) != s.charAt(i - 1))
				sb.append(s.charAt(i));
		}

		System.err.println("trim=" + sb.toString());
		return sb.toString();
	}
}