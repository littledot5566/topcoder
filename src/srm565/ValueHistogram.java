package srm565;

public class ValueHistogram {
	public String[] build(int[] values) {
		// count frequency
		int count[] = new int[10];
		for (int i : values) {
			count[i]++;
		}

		// get maximum frequency for graph height
		int h = 0;
		for (int i = 0; i < 10; i++) {
			System.err.print(count[i]);
			if (count[i] + 1 > h) {
				h = count[i] + 1;
			}
		}

		// chart frequency
		char graph[][] = new char[h][10];
		System.err.println(" h=" + h);

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < h - count[i]; j++) {
				graph[j][i] = '.';
			}
			for (int j = h - count[i]; j < h; j++) {
				graph[j][i] = 'X';
			}
		}

		// convert to string
		String ret[] = new String[h];
		for (int i = 0; i < h; i++) {
			ret[i] = new String(graph[i]);
		}
		return ret;
	}
}